module.exports = {
	/**
	 * Application configuration section
	 * http://pm2.keymetrics.io/docs/usage/application-declaration/
	 */
	apps: [
		// First application
		{
			name: 'theclimategame', // name of the process in PM2
			script: 'build/index.html',
			env_production: {
				NODE_ENV: 'production',
				PORT: 5173 // port the app will be launched on
			}
		}
	],

	/**
	 * Deployment section
	 * http://pm2.keymetrics.io/docs/usage/deployment/
	 */
	deploy: {
		production: {
			user: 'debian', // deployer user
			host: '51.210.244.16', // IP address of your server
			ref: 'origin/main', // the branch you want to deploy
			repo: 'git@gitlab.com:SoulNoraifu/the-climate-game.git', // the ssh git clone URL
			path: '/var/www/the-climate-game/', // the path where you want the project to be
			// code you want to run after the project has been pushed to your server
			'post-deploy':
				'npm install && npm run build && pm2 reload ecosystem.config.cjs --env production'
		}
	}
};